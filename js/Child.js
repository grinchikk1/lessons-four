class Child extends Parrent {
  constructor(name, age) {
    super(name);
    this.age = age;
    super.sayHello();
  }
  sayAll() {
    super.sayHello();
    super.sayMyAge();
  }
}
