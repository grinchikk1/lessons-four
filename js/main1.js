// function lesson() {
//   console.log("Hello", this);
// }

// const material = {
//   topic: "lesson about this, bind, apply, call",
//   number: 1,
//   welcomeFunc: lesson,
//   welcomeFuncNext: lesson.bind(document),
//   lessonInf: function (teatcherName, diff) {
//     console.group("Lessons info");
//     console.log(`Name of lesson is: ${this.topic}`);
//     console.log(`Number of lesson is: ${this.number}`);
//     console.log(`TeatcherName of lesson is: ${teatcherName}`);
//     console.log(`Difficult of lesson is: ${diff}`);
//     console.groupEnd();
//   },
// };

// const exams = {
//   topic: "Objects methods",
//   number: 3,
// };

// material.lessonInf.bind(exams)();
// const lessonInfExam = material.lessonInf.bind(exams, 'Maria Ivanivna', 'Hard');
// lessonInfExam()

// material.lessonInf.call(exams, 'Maria Ivanivna', 'Hard')
// material.lessonInf.apply(exams, ["Maria Ivanivna", "Hard"]);

// const array = [1, 2, 3, 4, 5];
// function multiplyNum(arr, n) {
//   return arr.map(function (num) {
//     return num * n;
//   });
// }

// Array.prototype.multiplyNum = function (num) {
//   return this.map(function (element) {
//     return element * num;
//   });
//   console.log("multyplyNum", this);
// };

// console.log(array.multiplyNum(5));
// console.log(multiplyNum(array, 5))

//========= Конструктор ==========//

// const student = {
//   name: "Petro",
//   cource: 2,
//   admissionYear: 2016,
// };

// let Func = function () {
//   this.name;
// };

// new Func();

// let Student = function (name) {
//   this.name = name;
//   this.course = "4";
//   let mark = "88";
// this.welcomeWords = function () {
//   return "Hello, i am a student " + this.name + ', my average mark is ' + mark;
// };
// return {
//   surname: "Danylenko"
// }
// };

// Student.prototype.welcomeWords = function () {
//   return "Hello, i am a student " + this.name + ", my average mark is " + mark;
// };

// let dmytro = new Student("Dmytro");
// let andriy = new Student("Andriy");

// console.log(andriy);
// console.log(dmytro);
// console.log(dmytro.name);
// console.log(dmytro.course);
// console.log(dmytro.welcomeWords());

// const user ={
//   name: 'Petro',
//   surname: 'Bilchenko'
// }

// const vitaliy = {
//   __proto__: user
// }

// console.log(vitaliy)

function Team(counts, name, goals, country) {
  this.country = country;
  this.name = name;
  this.counts = counts;
  this.goals = goals;
  // this.sickLive = "2";
  // this.result = function () {
  // if ((this.country = this.country + " league")) {
  //   return this.country
  // }
  // };
}

// average.prototype.goalsAverage =  function (matches) {
//   this.matches = matches;
// }

// Team.prototype.result = function () {
//   if (this.counts > 15) {
//     this.counts = this.counts - 1;
//     return this.counts;
//   } else {
//     console.log("That is fine");
//   }
// };

// Team.prototype.average = function () {
//   let average = this.goals / this.matches;
//   return average;
// };

// Team.prototype.sickLive = "1";
// const BM = new Games(5);
// const barcelona = new Team(20, "barcelona", 5, "Spain");
// const liverpool = new Team(25, "liverpool", 50, "GB");
// console.log(barcelona.result());
// console.log(barcelona.goalsAverage());
// console.log(liverpool.goalsAverage());

// const team1 = new Team(20, "barcelona", 5, "Spain");
// const games1 = new Games(5);
// console.log(liverpool.goalsAverage());

// const team = ["barcelona", "psg", "real"];

// function foo() {
//   console.log("Hello1");
// }

// function second() {
//   console.log("Hello2");
// }

// second.prototype.foo = function () {
//   console.log("call foo from second");
// };

// const myFunc = new second();

// myFunc.foo()
